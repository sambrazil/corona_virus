import React, {useState, useEffect} from 'react'
import ShowData from './ShowData'
import './App.css'
// api url
// https://coronavirus-19-api.herokuapp.com/countries
function CoronaVirus (){
  const [selectElems, setSelectElems] = useState([])
  const [country, setCountry] = useState('All')
  const [data, setData] = useState([])
 useEffect(()=>{
   (async function FetchData (){    
      
       if(country === "All" || !selectElems[0]){
          const response = await fetch('https://coronavirus-19-api.herokuapp.com/countries')
           const json = await response.json()

           if(!selectElems[0]){
            const selectElemsData = json.map( (data, i) =>( json[i].country))
            selectElemsData.sort()
            setSelectElems(selectElemsData)
          }
          if(country === "All"){
            let cases = 0
            let todayCases = 0
            let deaths = 0
            let todayDeaths = 0
            let recovered = 0
            let active = 0
            json.forEach((data, i)=>{
               cases+= parseInt(data.cases)
               todayCases+= parseInt(data.todayCases)
               deaths+= parseInt(data.deaths)
               todayDeaths+= parseInt(data.todayDeaths)
               recovered+= parseInt(data.recovered)
               active+= parseInt(data.active)
              
            })
             setData({
               cases: cases, todayCases: todayCases, deaths: deaths, todayDeaths: todayDeaths, recovered: recovered,
               active: active
            })
          }
          
       }
      else{
        const response = await fetch(`https://coronavirus-19-api.herokuapp.com/countries/${country}`)
        const json = await response.json()
        setData({
          cases: json.cases, todayCases: json.todayCases, deaths: json.deaths, todayDeaths: json.todayDeaths, 
          recovered: json.recovered, active: json.active
       })
        
      }
     

 })()

 },[country])
  return(
  <div>
    <div class="myAlign top"> 
  <label>เลือกประเทศ : </label>
  <select onChange={e=>{ setCountry(e.target.value)} }>
   <option value ='All'>All</option> 
   {selectElems.map( data =>(
     <option value ={data}>{data}</option>
   ))}
  </select> 
  </div>
   <ShowData data={data} country={country} />
   
  </div>
  
   
  )
}

export default CoronaVirus