import React from 'react';
import {Pie} from 'react-chartjs-2'
import './App.css'
function ShowData(props){
  const state = {
        labels: [ 'กำลังรักษา', 'เสียชีวิต',
                 'รักษาหายแล้ว'],
        datasets: [
          {
            label: 'Rainfall',
            backgroundColor: [
              
              'yellow',
              'red',
              'green',
              
            ],
            hoverBackgroundColor: [
            'black',
            'black',
            'black'
            ],
            data: [props.data.active, props.data.deaths, props.data.recovered]
          }
        ]
      }
 
 return (
     <div>
    <div class ='text myAlign top2'>ผู้ติดเชื้อทั้งหมด : {props.data.cases} | ผู้ติดเชื้อวันนี้ : {props.data.todayCases}</div>
   <div class = 'textDanger myAlign'>ผู้เสียชีวิตทั้งหมด : {props.data.deaths} | ผู้เสียชีวิตวันนี้ : {props.data.todayDeaths}</div>
   <div class= 'top'>
    <Pie
     
          data={state}
          options={{
            title:{
              display:true,
              text: props.country === 'All'? 
                     `อัพเดทสถานการณ์ 'ไวรัสโคโรน่า' ทั่วโลก `
                    : `อัพเดทสถานการณ์ ไวรัสโคโรน่า ประเทศ ${props.country}`,
              fontSize:30,
            },
            legend:{
              display:true,
              position:'left'
            }
          }}
        />
      </div>  
   
   
     </div>
 )
}
export default ShowData